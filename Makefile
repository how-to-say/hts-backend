HTS_FINAL_DIR=${HOME}/.how-to-say-runner

include $(HTS_FINAL_DIR)/config/how-to-say.cfg

run: _check_hts_files _install_dep
	@echo "Running"
	@pipenv run python app.py

test: _install_dep
	@echo "Running tests"
	@pipenv run pytest --cov=src tests/

_install_dep:
	@pipenv install --dev

_check_hts_files:
	@test -d $(HTS_FINAL_DIR) || ( echo "Error: Missing configs. Please run 'make set' in the localrun folder first" && false)

run_local: _check_hts_files
	@echo "Building local image"
	@docker build -t htw-backend:local-debug -f deploy/Dockerfile .
	@echo "Coping files to runner folder"
	@cp deploy/local/docker-compose.override.yml $(HTS_FINAL_DIR)
	@make -C $(HTS_FINAL_DIR) run_local


