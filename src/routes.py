"""
This class set API routes.
"""

from flask import Flask


def add_routes(app: Flask):
    """
    This method will add routes to our API

    Args:
        app (Flask): the target app to add routes
    """
    @app.route("/")
    def hello_world():
        """ This is a simple hello world"""
        return "Hello world"
