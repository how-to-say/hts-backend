from flask import Flask
from src.routes import add_routes


app = Flask(__name__)
add_routes(app)
