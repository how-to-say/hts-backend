"""
This module run the Flask app locally
"""

import os

from src import app


if __name__ == "__main__":
    port = int(os.getenv("LOCAL_API_PORT"))
    app.run(port=port)
